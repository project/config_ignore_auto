Config Ignore Auto

This module, when both enabled and active, keep track of what configs were
edited and make sure those are ignored from that moment on future config
imports.

The module is inactive when your first enable it. Either activate it through
config although you will likely want to do this by overriding the config on your
production site with:

<?php
$config['config_ignore_auto.settings']['status'] = TRUE;
?>

The main use case for this module is for those times where the end user of the
site knows and wants to change stuff (views, webforms, blocks, etc.). Instead of
ignoring specific webforms by demand of the end user, or ignoring whole set of
configs, this module assumes that every config the client changes will be
ignored from that moment onwards.

So when something is changed, by ignoring it we are protecting those changes
from being accidentally overwritten on future imports.

The module detects when the configuration is being synchronised so those configs
are not auto-ignored as well as when modules are enabled or disabled.

You might still need to put back those changes back to code or merge them with
your own if you need to deploy your own config changes but that's a manual
process for you to decide how best to handle. You might need to remove the
1ignore from this module's settings.

