<?php

/**
 * @file
 */

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\config_ignore\ConfigIgnoreConfig;

/**
 * Implements hook_form_alter().
 */
function config_ignore_auto_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $config = FALSE;

  // Try to dectect config forms.
  $f = $form_state->getFormObject();
  if ($f instanceof EntityForm) {
    $entity = $f->getEntity();
    if ($entity instanceof ConfigEntityInterface && !$entity->isNew()) {
      $config = TRUE;
    }
  }
  elseif (method_exists($f, 'getEditableConfigNames')) {
    $config = TRUE;
  }

  if ($config && config_ignore_auto_is_enabled() && \Drupal::state()->get('system.maintenance_mode', FALSE)) {
    \Drupal::service('messenger')->addWarning('This config form will not be auto ignored because the site is operating in maintenance mode.');
  }
}

/**
 * Whether this module should be auto-tracking config changes.
 *
 * Depends on several variables.
 */
function config_ignore_auto_is_enabled($enable = TRUE) {
  static $enabled = TRUE;
  $config_ignore_settings = \Drupal::config('config_ignore_auto.settings');

  if (!$enable) {
    $enabled = FALSE;
  }

  return $config_ignore_settings->get('status') && $enabled && !\Drupal::isConfigSyncing();
}

/**
 * Logic for whitelisted and own module's settings that should never be ignored.
 */
function config_ignore_auto_should_ignore($config_name) {
  // If the site is in maintenance mode, do not auto ignore.
  // This is mostly useful for db updates, where maintenance mode is set, so
  // that even with this module enabled, like on production systems,
  // module update that change configs are not going to be auto ignored.
  // If the site is manually put into maintenance mode, configs will not be auto
  // ignored, so beware of this. A status message will be displayed in those
  // cases.
  // @see DbUpdateController::triggerBatch()
  $maintenance_mode = \Drupal::state()->get('system.maintenance_mode', FALSE);
  if ($maintenance_mode) {
    return FALSE;
  }

  if ($config_name == 'config_ignore_auto.settings') {
    return FALSE;
  }

  $whitelist = \Drupal::config('config_ignore_auto.settings')->get('whitelist_config_entities');
  if (in_array($config_name, $whitelist)) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Implements hook_module_preinstall().
 */
function config_ignore_auto_module_preinstall($module) {
  config_ignore_auto_is_enabled(FALSE);
}

/**
 * Implements hook_module_preuninstall().
 */
function config_ignore_auto_module_preuninstall($module) {
  config_ignore_auto_is_enabled(FALSE);
}

/**
 * Implements hook_config_ignore_ignored_alter().
 *
 * Ignores all configured configurations.
 *
 * Note: config_ignore_auto.settings:ignored_config_entities will be always
 * ignored on both import and export.
 */
function config_ignore_auto_config_ignore_ignored_alter(ConfigIgnoreConfig $ignored) {
  $config = \Drupal::config('config_ignore_auto.settings');
  $ignores = $config->get('ignored_config_entities');
  if (!empty($ignores)) {
    $config_operations = $config->get('direction_operations');
    foreach (array_filter($config_operations) as $v) {
      [$direction, $operation] = preg_split('/_/', $v);
      $list = $ignored->getList($direction, $operation);
      $ignored->setList($direction, $operation, array_merge($list, $ignores));
    }
  }

  $config_ignore_auto_ignore = ['config_ignore_auto.settings:ignored_config_entities'];
  $operations = ['import_create', 'import_update', 'import_delete', 'export_create', 'export_update', 'export_delete'];

  foreach ($operations as $v) {
    [$direction, $operation] = preg_split('/_/', $v);
    $list = $ignored->getList($direction, $operation);
    $ignored->setList($direction, $operation, array_merge($list, $config_ignore_auto_ignore));
  }
}

/**
 * Implements hook_module_implements_alter().
 *
 * Make sure we run after config_ignore.
 */
function config_ignore_auto_module_implements_alter(&$implementations, $hook) {
  if ($hook == 'form_config_admin_import_form_alter') {
    $group = $implementations['config_ignore_auto'];
    unset($implementations['config_ignore_auto']);
    $implementations['config_ignore_auto'] = $group;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function config_ignore_auto_form_config_admin_import_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (!empty($form['ignored'])) {
    $form['ignored']['#caption'] = t(
      '@caption<h3>Some of them could also be auto ignored beacuse of to the <a href="@url">Config Ignore Auto Settings</a></h3>',
      [
        '@caption' => $form['ignored']['#caption'],
        '@url' => Url::fromRoute('config_ignore_auto.settings')->toString(),
      ]
    );
  }
}
