<?php

namespace Drupal\config_ignore_auto\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a setting UI for Config Ignore.
 *
 * @package Drupal\config_ignore\Form
 */
class Settings extends ConfigFormBase {

  /**
   * The Default Plugin Manager (Optional)
   *
   * @var Drupal\Core\Plugin\DefaultPluginManager
   */
  protected $configFilterPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);

    if ($container->has('plugin.manager.config_filter')) {
      $instance->setConfigFilterPluginManager($container->get('plugin.manager.config_filter'));
    }
    return $instance;
  }

  /**
   * Sets the Default Plugin Manager.
   *
   * @param \Drupal\Core\Plugin\DefaultPluginManager $plugin_manager
   *   The Default Plugin Manager.
   *
   * @return $this
   */
  public function setConfigFilterPluginManager(DefaultPluginManager $plugin_manager) {
    $this->configFilterPluginManager = $plugin_manager;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'config_ignore_auto.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_ignore_auto_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?Request $request = NULL) {
    $description = $this->t('
<b>Note: <em>config_ignore_auto.settings:ignored_config_entities</em> (this config field) will always be ignored.</b></br>
One configuration name per line.<br />
Examples: <ul>
<li>user.settings</li>
<li>views.settings</li>
<li>contact.settings</li>
<li>webform.webform.* (will ignore all config entities that starts with <em>webform.webform</em>)</li>
<li>*.contact_message.custom_contact_form.* (will ignore all config entities that starts with <em>.contact_message.custom_contact_form.</em> like fields attached to a custom contact form)</li>
<li>* (will ignore everything)</li>
<li>~webform.webform.contact (will force import for this configuration, even if ignored by a wildcard)</li>
<li>user.mail:register_no_approval_required.body (will ignore the body of the no approval required email setting, but will not ignore other user.mail configuration.)</li>
</ul>');

    $description_whitelist = $this->t('One configuration name per line. <b>This one only accepts exact config names.</b><br />
Examples: <ul>
<li>core.extension</li>
</ul>');

    $config_ignore_settings_immutable = $this->configFactory()->get('config_ignore_auto.settings');
    $config_ignore_settings = $this->config('config_ignore_auto.settings');

    $form['status'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Status'),
    ];

    $status = $config_ignore_settings_immutable->get('status') ? $this->t('Enabled') : $this->t('Disabled');
    $status .= $config_ignore_settings->get('status') != $config_ignore_settings_immutable->get('status') ? $this->t(' (overridden)') : '';
    $form['status']['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#prefix' => $this->t('<p>Current status: <b>:status</b></p>', [':status' => $status]),
      '#default_value' => $config_ignore_settings->get('status'),
      '#description' => $this->t('Check this box if you want to enable auto config ignoring. Uncheck to disable.<br/><b>Note:</b> The status is just for auto-ignoring changes, what\'s currently auto-ignored will continue to be ignored as per configured.'),
    ];

    $form['ignored_config_entities'] = [
      '#type' => 'textarea',
      '#rows' => 25,
      '#title' => $this->t('Configuration entity names to ignore'),
      '#description' => $description,
      '#default_value' => implode(PHP_EOL, $config_ignore_settings->get('ignored_config_entities') ?? []),
      '#size' => 60,
    ];

    $form['direction_operations'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select when the auto ignored config should be ignored'),
      '#description' => $this->t('<b>Note: <em>config_ignore_auto.settings:ignored_config_entities</em> ("' . $form['ignored_config_entities']['#title'] . '" field above) will always be ignored for every operation.</b>'),
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#options' => [
        'import_create' => $this->t('<b>Import -></b> Create'),
        'import_update' => $this->t('<b>Import -></b> Update'),
        'import_delete' => $this->t('<b>Import -></b> Delete'),
        'export_create' => $this->t('<b>Export -></b> Create'),
        'export_update' => $this->t('<b>Export -></b> Update'),
        'export_delete' => $this->t('<b>Export -></b> Delete'),
      ],
      '#default_value' => $config_ignore_settings->get('direction_operations') ?? [],
    ];

    $form['whitelist_config_entities'] = [
      '#type' => 'textarea',
      '#rows' => 10,
      '#title' => $this->t('Configuration entity that should never be ignored'),
      '#description' => $description_whitelist,
      '#default_value' => implode(PHP_EOL, $config_ignore_settings->get('whitelist_config_entities')),
      '#size' => 60,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config_ignore_settings = $this->config('config_ignore_auto.settings');
    $ignored_config_entities_array = preg_split("/[\r\n]+/", $values['ignored_config_entities']);
    $ignored_config_entities_array = array_filter($ignored_config_entities_array);
    $ignored_config_entities_array = array_values($ignored_config_entities_array);
    $config_ignore_settings->set('ignored_config_entities', $ignored_config_entities_array);
    $whitelist_config_entities_array = preg_split("/[\r\n]+/", $values['whitelist_config_entities']);
    $whitelist_config_entities_array = array_filter($whitelist_config_entities_array);
    $whitelist_config_entities_array = array_values($whitelist_config_entities_array);
    $config_ignore_settings->set('whitelist_config_entities', $whitelist_config_entities_array);
    $config_ignore_settings->set('status', $values['status']);
    $config_ignore_settings->set('direction_operations', array_values(array_filter($values['direction_operations'])));
    $config_ignore_settings->save();
    parent::submitForm($form, $form_state);

    // Clear the config_filter plugin cache.
    if (isset($this->configFilterPluginManager)) {
      $this->configFilterPluginManager->clearCachedDefinitions();
    }
  }

}
