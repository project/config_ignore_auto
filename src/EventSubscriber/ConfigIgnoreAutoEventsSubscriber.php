<?php

namespace Drupal\config_ignore_auto\EventSubscriber;

use Drupal\Core\Config\ConfigCrudEvent;
use Drupal\Core\Config\ConfigEvents;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class EntityTypeSubscriber.
 *
 * @package Drupal\custom_events\EventSubscriber
 */
class ConfigIgnoreAutoEventsSubscriber implements EventSubscriberInterface {

  /**
   * The system theme config object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Default Plugin Manager (Optional)
   *
   * @var Drupal\Core\Plugin\DefaultPluginManager
   */
  protected $configFilterPluginManager;

  /**
   * Constructs a DefaultNegotiator object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * Sets the Default Plugin Manager.
   *
   * @param \Drupal\Core\Plugin\DefaultPluginManager $plugin_manager
   *   The Default Plugin Manager.
   *
   * @return $this
   */
  public function setConfigFilterPluginManager(DefaultPluginManager $plugin_manager) {
    $this->configFilterPluginManager = $plugin_manager;
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * @return array
   *   The event names to listen for, and the methods that should be executed.
   */
  public static function getSubscribedEvents() {
    return [
      ConfigEvents::SAVE => 'configSave',
      ConfigEvents::DELETE => 'configDelete',
    ];
  }

  /**
   * React to a config object being saved.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Config crud event.
   */
  public function configSave(ConfigCrudEvent $event) {
    $config = $event->getConfig();
    $config_name = $config->getName();

    if (config_ignore_auto_is_enabled() && config_ignore_auto_should_ignore($config_name)) {
      $config_ignore_settings = $this->configFactory->getEditable('config_ignore_auto.settings');
      $ignored_keys = $config_ignore_settings->get('ignored_config_entities');
      $ignored_keys[] = $config_name;
      $ignored_keys = array_unique($ignored_keys);
      asort($ignored_keys);
      $config_ignore_settings->set('ignored_config_entities', $ignored_keys);
      $config_ignore_settings->save();

      if (isset($this->configFilterPluginManager)) {
        $this->configFilterPluginManager->clearCachedDefinitions();
      }
    }
  }

  /**
   * React to a config object being deleted.
   *
   * @param \Drupal\Core\Config\ConfigCrudEvent $event
   *   Config crud event.
   */
  public function configDelete(ConfigCrudEvent $event) {
    $this->configSave($event);
  }

}
